ifeq ($(OS),Windows_NT)
	CLEANUP=del /F /Q
	MKDIR=mkdir
	TARGET_EXTENSION=exe
	HOST_OS=$(OS)
else
    HOST_OS := $(shell uname -s)
    ifeq ($(HOST_OS),Linux)
		CLEANUP=rm -f
		MKDIR=mkdir -p
    endif
    ifeq ($(HOST_OS),Darwin)
		CLEANUP=rm -f
		MKDIR=mkdir -p
    endif
endif

BUILD_DIR = Debug
PROJECT = otb_app

CC = arm-none-eabi-gcc
LD = arm-none-eabi-ld 
AR = arm-none-eabi-ar
AS = arm-none-eabi-as
CP = arm-none-eabi-objcopy
OD = arm-none-eabi-objdump
SZ = arm-none-eabi-size
RD = arm-none-eabi-readelf

# arm-none-eabi-gcc flags
DEFS = -DUSE_HAL_DRIVER -DDEBUG -DSTM32F407xx -DUSE_FULL_LL_DRIVER
STD = -std=gnu11
OPTIMIZE = -O0
DEBUG_FLAGS = -g3
WARNINGS = -Wall
MCU = cortex-m4
MCU_TARGET_FLAG = -mcpu=$(MCU) 
MCU_FLAGS = -fstack-usage --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb
CONTROL_OPTIMIZATION = -ffunction-sections -fdata-sections

# MCU flash and ram size (in bytes) in hexa
MCU_FLASH_SIZE = 0x40000 # 256kB, the application can have, at most, 256kB
MCU_RAM_SIZE = 0x30000 # 192kB

## Directory for the objects (.o). 
OBJ_DIR = $(BUILD_DIR)

## The PATH where the other repositories of the project are located (stm32cube and freertos plus libraries )
OBCS_TOOLS_PATH = ../obcs_tools
OBCS_API_PATH = ../obcs_api

## STMicroelectronics libraries ###############################################
DIR_STM32LIBS = $(OBCS_TOOLS_PATH)/0.STM32Cube_FW_F4_V1.26.0

# Dir of the sources of FreeRTOS inside the stm32 libraries.
# The ethernet driver used is not the one that comes from the STM32Cube FW. 
# This project uses a Ethernet HAL driver customized/ported by the developers of 
# FreeRTOS+Plus to the STM32F4xx MCUs
DIR_FREERTOS = Middlewares/Third_Party/FreeRTOS/Source
# Dir of the sources of FatFS inside the stm32 libraries.
DIR_FATFS = Middlewares/Third_Party/FatFs/src

SRC_STM32LIBS =	$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_exti.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_qspi.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_spi.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_adc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_tim.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_dma.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_exti.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_gpio.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_rcc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usart.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_utils.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_crc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_dac.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_dma2d.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_i2c.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_lptim.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_pwr.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_rng.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_rtc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c \
				$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/croutine.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/event_groups.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/list.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/queue.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/stream_buffer.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/tasks.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/timers.c \
				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/portable/GCC/ARM_CM4F/port.c \
				$(DIR_STM32LIBS)/$(DIR_FATFS)/diskio.c \
				$(DIR_STM32LIBS)/$(DIR_FATFS)/ff_gen_drv.c \
				$(DIR_STM32LIBS)/$(DIR_FATFS)/ff.c \
				$(DIR_STM32LIBS)/$(DIR_FATFS)/option/unicode.c \
#				$(DIR_STM32LIBS)/$(DIR_FREERTOS)/portable/MemMang/heap_4.c \
# NOTE: exclude heap_4 and only include heap_useNewlib. 
# See here: https://nadler.com/embedded/newlibAndFreeRTOS.html

INC_STM32LIBS =	-I$(DIR_STM32LIBS)/Drivers/CMSIS/Device/ST/STM32F4xx/Include \
				-I$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Inc \
				-I$(DIR_STM32LIBS)/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy \
				-I$(DIR_STM32LIBS)/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F \
				-I$(DIR_STM32LIBS)/Middlewares/Third_Party/FreeRTOS/Source/include \
				-I$(DIR_STM32LIBS)/$(DIR_FREERTOS)/portable/GCC/ARM_CM4F \
				-I$(DIR_STM32LIBS)/$(DIR_FREERTOS)/include \
				-I$(DIR_STM32LIBS)/$(DIR_FATFS) \
				-I$(DIR_STM32LIBS)/Drivers/CMSIS/Include \
#				-I$(DIR_STM32LIBS)/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 \

# Get the name of the sources in a variable
NAME_SRC_STM32LIBS = $(notdir $(SRC_STM32LIBS))
## Get the directories of the sources in a variable
DIR_SRC_STM32LIBS = $(dir $(SRC_STM32LIBS))
# Build the directories path of the objects
DIR_OBJ_STM32LIBS = $(DIR_SRC_STM32LIBS:$(DIR_STM32LIBS)/%=$(OBJ_DIR)/%)
# Make a "join" of the dirs an the source names
JOIN_STM32LIBS = $(join $(DIR_OBJ_STM32LIBS), $(NAME_SRC_STM32LIBS))
# Build the object names with its correspondent directory path.
OBJ_STM32LIBS = $(JOIN_STM32LIBS:%.c=%.o)

## cJSON library ########################################################
DIR_CJSON = $(OBCS_TOOLS_PATH)/cJSON

SRC_CJSON = $(DIR_CJSON)/cJSON.c \
			$(DIR_CJSON)/cJSON_Utils.c 

INC_CJSON = -I$(DIR_CJSON)

# Get the name of the sources in a variable
NAME_SRC_CJSON = $(notdir $(SRC_CJSON))
## Get the directories of the sources in a variable
DIR_SRC_CJSON = $(dir $(SRC_CJSON))
# Build the directories path of the objects
DIR_OBJ_CJSON = $(DIR_SRC_CJSON:$(DIR_CJSON)/%=$(OBJ_DIR)/%)
# Make a "join" of the dirs an the source names
JOIN_CJSON = $(join $(DIR_OBJ_CJSON), $(NAME_SRC_CJSON))
# Build the object names with its correspondent directory path.
OBJ_CJSON = $(JOIN_CJSON:%.c=%.o)

## Freertos+FAT library ########################################################
DIR_FREERTOS_PLUS_FAT = $(OBCS_TOOLS_PATH)/Lab-Project-FreeRTOS-FAT
SRC_FREERTOS_PLUS_FAT = $(DIR_FREERTOS_PLUS_FAT)/ff_crc.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_dir.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_error.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_fat.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_file.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_format.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_ioman.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_locking.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_memory.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_stdio.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_string.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_sys.c \
						$(DIR_FREERTOS_PLUS_FAT)/ff_time.c \
#						$(DIR_FREERTOS_PLUS_FAT)/ff_dev_support.c \

INC_FREERTOS_PLUS_FAT = -I$(DIR_FREERTOS_PLUS_FAT)/include

# Get the name of the sources in a variable
NAME_SRC_FREERTOS_PLUS_FAT = $(notdir $(SRC_FREERTOS_PLUS_FAT))
## Get the directories of the sources in a variable
DIR_SRC_FREERTOS_PLUS_FAT = $(dir $(SRC_FREERTOS_PLUS_FAT))
# Build the directories path of the objects
DIR_OBJ_FREERTOS_PLUS_FAT = $(DIR_SRC_FREERTOS_PLUS_FAT:$(DIR_FREERTOS_PLUS_FAT)/%=$(OBJ_DIR)/%)
# Make a "join" of the dirs an the source names
JOIN_FREERTOS_PLUS_FAT = $(join $(DIR_OBJ_FREERTOS_PLUS_FAT), $(NAME_SRC_FREERTOS_PLUS_FAT))
# Build the object names with its correspondent directory path.
OBJ_FREERTOS_PLUS_FAT = $(JOIN_FREERTOS_PLUS_FAT:%.c=%.o)

## Freertos+TCP library ########################################################
# https://github.com/aws/amazon-freertos/pull/1638
DIR_FREERTOS_PLUS_TCP = $(OBCS_TOOLS_PATH)/1.FreeRTOS-Plus_V20210400

SRC_FREERTOS_PLUS_TCP = $(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_ARP.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_DHCP.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_DNS.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_IP.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_Sockets.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/NetworkInterface/STM32Fxx/NetworkInterface.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/NetworkInterface/STM32Fxx/stm32fxx_hal_eth.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/NetworkInterface/Common/phyHandling.c \
						$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/BufferManagement/BufferAllocation_2.c \

INC_FREERTOS_PLUS_TCP = -I$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/include \
						-I$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/NetworkInterface/STM32Fxx \
						-I$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/NetworkInterface/include \
						-I$(DIR_FREERTOS_PLUS_TCP)/Source/FreeRTOS-Plus-TCP/portable/Compiler/GCC \

# Get the name of the sources in a variable
NAME_SRC_FREERTOS_PLUS_TCP = $(notdir $(SRC_FREERTOS_PLUS_TCP))
## Get the directories of the sources in a variable
DIR_SRC_FREERTOS_PLUS_TCP = $(dir $(SRC_FREERTOS_PLUS_TCP))
# Build the directories path of the objects
DIR_OBJ_FREERTOS_PLUS_TCP = $(DIR_SRC_FREERTOS_PLUS_TCP:$(DIR_FREERTOS_PLUS_TCP)/%=$(OBJ_DIR)/%)
# Make a "join" of the dirs an the source names
JOIN_FREERTOS_PLUS_TCP = $(join $(DIR_OBJ_FREERTOS_PLUS_TCP), $(NAME_SRC_FREERTOS_PLUS_TCP))
# Build the object names with its correspondent directory path.
OBJ_FREERTOS_PLUS_TCP = $(JOIN_FREERTOS_PLUS_TCP:%.c=%.o)

## FreeRTOS Helpers ###########################################################
# From here, compile the heap_useNewlib_ST.c. In this source code there is a 
# thread safe implementation of _sbrk. This function is used by malloc.
DIR_FREERTOS_HELPERS = $(OBCS_TOOLS_PATH)/FreeRTOS_helpers

SRC_FREERTOS_HELPERS = $(DIR_FREERTOS_HELPERS)/heap_useNewlib_ST.c \

# Get the name of the sources in a variable
NAME_SRC_FREERTOS_HELPERS = $(notdir $(SRC_FREERTOS_HELPERS))
## Get the directories of the sources in a variable
DIR_SRC_FREERTOS_HELPERS = $(dir $(SRC_FREERTOS_HELPERS))
# Build the directories path of the objects
DIR_OBJ_FREERTOS_HELPERS = $(DIR_SRC_FREERTOS_HELPERS:$(DIR_FREERTOS_HELPERS)/%=$(OBJ_DIR)/%)
# Make a "join" of the dirs an the source names
JOIN_FREERTOS_HELPERS = $(join $(DIR_OBJ_FREERTOS_HELPERS), $(NAME_SRC_FREERTOS_HELPERS))
# Build the object names with its correspondent directory path.
OBJ_FREERTOS_HELPERS = $(JOIN_FREERTOS_HELPERS:%.c=%.o)

## OBCs API ###################################################################
DIR_API = $(OBCS_API_PATH)
DIR_BOARD_CORE = Core

SRC_API = 	$(DIR_API)/API/common/api_init.c \
			$(DIR_API)/API/common/FreeRTOS_definitions.c \
			$(DIR_API)/API/common/printf-stdarg.c \
			$(DIR_API)/API/fsm/api_fsm.c \
			$(DIR_API)/API/flash/api_flash.c \
			$(DIR_API)/API/datetime/api_datetime.c \
			$(DIR_API)/API/serial_log/api_serial_log.c \
			$(DIR_API)/API/gpio/api_gpio.c \
			$(DIR_API)/API/fs/api_fs.c \
			$(DIR_API)/API/fs/fatfs/api_fatfs.c \
			$(DIR_API)/API/fs/ff_fat/api_ff_fat.c \
			$(DIR_API)/API/iap/api_iap.c \
			$(DIR_API)/API/network/osotb/api_osotb.c \
			$(DIR_API)/API/network/osotb/api_osotb_sv.c \
			$(DIR_API)/API/network/osotb/api_osotb_rx.c \
			$(DIR_API)/API/network/common/api_network.c \
			$(DIR_API)/API/network/tcp/FreeRTOS_TCP_server.c \
			$(DIR_API)/API/network/ftp/FreeRTOS_FTP_server.c \
			$(DIR_API)/API/network/ftp/FreeRTOS_FTP_commands.c \
			$(DIR_API)/API/network/ftp/api_ftp.c \
			$(DIR_API)/API/tcpip/api_tcpip.c \
			$(DIR_API)/API/logger/api_logger.c \
			$(DIR_API)/Board/board.c \
			$(DIR_API)/Board/board_flash.c \
			$(DIR_API)/Board/board_gpio.c \
			$(DIR_API)/Board/board_usart.c \
			$(DIR_API)/Board/board_eth.c \
			$(DIR_API)/Board/board_sd_diskio.c \
			$(DIR_API)/Board/board_mb85rs_diskio.c \
			$(DIR_API)/Board/board_random_number.c \
			$(DIR_API)/Board/board_timer.c \
			$(DIR_API)/Board/$(DIR_BOARD_CORE)/Src/stm32f4xx_hal_msp.c \
			$(DIR_API)/Board/$(DIR_BOARD_CORE)/Src/stm32f4xx_hal_timebase_tim.c \
			$(DIR_API)/Board/$(DIR_BOARD_CORE)/Src/stm32f4xx_it.c \
			$(DIR_API)/Board/$(DIR_BOARD_CORE)/Src/syscalls.c \
			$(DIR_API)/Board/$(DIR_BOARD_CORE)/Src/system_stm32f4xx.c \
#			$(DIR_API)/API/cfgkeys/api_cfgkeys.c \

INC_API =	-I$(DIR_API) \
			-I$(DIR_API)/API \
			-I$(DIR_API)/API/common/Inc \
			-I$(DIR_API)/API/fsm/Inc \
			-I$(DIR_API)/API/datetime/Inc \
			-I$(DIR_API)/API/flash/Inc \
			-I$(DIR_API)/API/fs/Inc \
			-I$(DIR_API)/API/fs/fatfs/Inc \
			-I$(DIR_API)/API/fs/ff_fat/Inc \
			-I$(DIR_API)/API/network/common/Inc \
			-I$(DIR_API)/API/network/priv_inc \
			-I$(DIR_API)/API/network \
			-I$(DIR_API)/API/network/osotb \
			-I$(DIR_API)/API/ftp/Inc \
			-I$(DIR_API)/API/logger/Inc \
			-I$(DIR_API)/API/gpio/Inc \
			-I$(DIR_API)/API/iap/Inc \
			-I$(DIR_API)/API/serial_log/Inc \
			-I$(DIR_API)/API/tcpip/Inc \
			-I$(DIR_API)/Board/Inc \
			-I$(DIR_API)/Board/$(DIR_BOARD_CORE)/Inc \
			-I$(DIR_API)/API/cfgkeys/Inc \
			
# Get the name of the sources in a variable
NAME_SRC_API = $(notdir $(SRC_API))
## Get the directories of the sources in a variable
DIR_SRC_API = $(dir $(SRC_API))
# Build the directories path of the objects
DIR_OBJ_API = $(DIR_SRC_API:$(DIR_API)/%=$(OBJ_DIR)/%)
# Make a "join" of the dirs an the source names
JOIN_API = $(join $(DIR_OBJ_API), $(NAME_SRC_API))
# Build the object names with its correspondent directory path.
OBJ_API = $(JOIN_API:%.c=%.o)

## OBCs API assembly sources
SRC_API_ASM = $(DIR_API)/Board/$(DIR_BOARD_CORE)/Startup/startup_stm32f407xx.s

# Get the name of the sources in a variable
NAME_SRC_API_ASM = $(notdir $(SRC_API_ASM))
# Get the directories of the sources in a variable
DIR_SRC_API_ASM = $(dir $(SRC_API_ASM))
# Build the directories path of the objects
DIR_OBJ_API_ASM = $(addprefix $(OBJ_DIR)/, $(DIR_SRC_API_ASM))
# Make a "join" of the dirs an the source names
JOIN_API_ASM = $(join $(DIR_OBJ_API_ASM), $(NAME_SRC_API_ASM))
# Build the object names with its correspondent directory path.
OBJ_API_ASM = $(JOIN_API_ASM:%.s=%.o)

## APP Sources ################################################################
SRC_APPENDIX = boot
DIR_SRC = Src

## C Sources
SRC_C = 	$(DIR_SRC)/System/system.c \
			$(DIR_SRC)/obcs_api_cfg/board_config.c \
			$(DIR_SRC)/Net/net.c \
			$(DIR_SRC)/Net/net_api_cb.c \
			$(DIR_SRC)/Net/net_ftp.c \
			$(DIR_SRC)/Net/net_osotb.c \
			$(DIR_SRC)/Net/net_taskmsg.c \
			$(DIR_SRC)/App/app.c \
			$(DIR_SRC)/App/app_taskmsg.c \
			$(DIR_SRC)/FDIR/fdir.c \

INC =	-I$(DIR_SRC) \
		-I$(DIR_SRC)/App/Inc \
		-I$(DIR_SRC)/System/Inc \
		-I$(DIR_SRC)/Net/Inc \
		-I$(DIR_SRC)/FDIR/Inc \
		-I$(DIR_SRC)/obcs_api_cfg \

# Get the name of the sources in a variable
NAME_SRC_C = $(notdir $(SRC_C))
# Get the directories of the sources in a variable
DIR_SRC_C = $(dir $(SRC_C))
# Build the directories path of the objects
DIR_OBJ_C = $(addprefix $(OBJ_DIR)/, $(DIR_SRC_C))
# Make a "join" of the dirs and the source names
JOIN_C = $(join $(DIR_OBJ_C), $(NAME_SRC_C))
# Build the object names with its correspondent directory path.
OBJ_C = $(JOIN_C:%.c=%.o)

## GCC C Flags ###############################################################
CFLAGS = $(MCU_TARGET_FLAG) $(STD) $(DEBUG_FLAGS) $(DEFS) -c $(INC_API) $(INC_STM32LIBS) $(INC) $(INC_FREERTOS_PLUS_TCP) $(INC_FREERTOS_PLUS_FAT) $(INC_CJSON) $(OPTIMIZE) $(CONTROL_OPTIMIZATION) $(WARNINGS) $(MCU_FLAGS)
FREERTOS_PLUS_TCP_CFLAGS = $(CFLAGS)
STM32LIBS_CFLAGS = $(CFLAGS)
API_CFLAGS = $(CFLAGS)
CJSON_CFLAGS = $(CFLAGS)
FREERTOS_PLUS_FAT_CFLAGS = $(CFLAGS)
FREERTOS_HELPERS_CFLAGS = $(CFLAGS)

SFLAGS = $(MCU_TARGET_FLAG) $(STD) $(DEBUG_FLAGS) -c -x assembler-with-cpp -MMD -MP -MF
API_SFLAGS = $(SFLAGS)

## Concatenations #############################################################
## Concat all the objects:
OBJECTS = $(OBJ_C) $(OBJ_STM32LIBS) $(OBJ_FREERTOS_PLUS_TCP) $(OBJ_FREERTOS_PLUS_FAT) $(OBJ_FREERTOS_HELPERS) $(OBJ_API) $(OBJ_API_ASM) $(OBJ_CJSON)

## Concat all the object dirs:
DIR_OBJECTS = $(DIR_OBJ_ASM) $(DIR_OBJ_C) $(DIR_OBJ_STM32LIBS) $(DIR_OBJ_FREERTOS_PLUS_TCP) $(DIR_OBJ_FREERTOS_PLUS_FAT) $(DIR_OBJ_FREERTOS_HELPERS) $(DIR_OBJ_API) $(DIR_OBJ_API_ASM) $(DIR_OBJ_CJSON)

## Linker scripts used ########################################################
LINKER_SCRIPT = STM32F407xx_APP_FLASH.ld

## Output files ###############################################################
## Name of the map files generated by the compiler:
MAP_FILE = $(BUILD_DIR)/$(PROJECT).map

## Name of the .elf files
ELF = $(BUILD_DIR)/$(PROJECT).elf

## Name of the binaries
BIN = $(BUILD_DIR)/$(PROJECT).bin

## Declare the PHONY targets (this targets don't represent files)
.PHONY = clean rebuild all

### Build all
all: $(BIN)
	@echo $(BIN) built in $(HOST_OS) system

## Rules that build the .bin files of boot code separately.
# Also prints the size of the binaries
$(BIN): $(ELF)
	$(CP) -O binary $< $@

## Rules that build the .elf files
$(ELF): $(OBJECTS)
	$(CC) -o $@ $^ $(MCU_TARGET_FLAG) --specs=nosys.specs -Wl,--gc-sections -Wl,-Map=$(MAP_FILE) -T$(LINKER_SCRIPT) -static --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Wl,--start-group -lc -lm -Wl,--end-group
	$(SZ) $@
	@./get-fw-size $@ $(MCU_FLASH_SIZE) $(MCU_RAM_SIZE) > get-fw-size-out.txt
	@cat get-fw-size-out.txt
	@rm get-fw-size-out.txt

# Build the objects of the stm32 libraries
$(OBJ_STM32LIBS) : $(SRC_STM32LIBS)
	$(CC) $(@:$(OBJ_DIR)/%.o=$(DIR_STM32LIBS)/%.c) $(STM32LIBS_CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

# Build the objects of the cJSON library
$(OBJ_CJSON) : $(SRC_CJSON)
	$(CC) $(@:$(OBJ_DIR)/%.o=$(DIR_CJSON)/%.c) $(CJSON_CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

# Build the objects of the OBCs API
$(OBJ_API) : $(SRC_API)
	$(CC) $(@:$(OBJ_DIR)/%.o=$(DIR_API)/%.c) $(API_CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

$(OBJ_API_ASM) : $(SRC_API_ASM)
	$(CC) $(API_SFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@  $(@:$(OBJ_DIR)/%.o=%.s)

# Build the objects of the app code
$(OBJ_C) : $(SRC_C)
	$(CC) $(@:$(OBJ_DIR)/%.o=%.c) $(CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

# Build the objects of the FreeRTOS+FAT library
$(OBJ_FREERTOS_PLUS_FAT) : $(SRC_FREERTOS_PLUS_FAT)
	$(CC) $(@:$(OBJ_DIR)/%.o=$(DIR_FREERTOS_PLUS_FAT)/%.c) $(FREERTOS_PLUS_FAT_CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

# Build the objects of the FreeRTOS+TCP library
$(OBJ_FREERTOS_PLUS_TCP) : $(SRC_FREERTOS_PLUS_TCP)
	$(CC) $(@:$(OBJ_DIR)/%.o=$(DIR_FREERTOS_PLUS_TCP)/%.c) $(FREERTOS_PLUS_TCP_CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

# Build the objects of the FreeRTOS Helpers library
$(OBJ_FREERTOS_HELPERS) : $(SRC_FREERTOS_HELPERS)
	$(CC) $(@:$(OBJ_DIR)/%.o=$(DIR_FREERTOS_HELPERS)/%.c) $(FREERTOS_HELPERS_CFLAGS) -MMD -MP -MF$(@:%.o=%.d) -MT$@ -o $@

# Build the directories where are stored the build objects
DUMMY_BUILD_FOLDER := $(shell $(MKDIR) $(DIR_OBJECTS))

## Clear the directory where all the build outputs is stored
clean: 
	$(CLEANUP) -R $(BUILD_DIR)

## Rebuild the sources (clean and build)
rebuild: clean all

## END OF BUILD SECTION #######################################################
