/*
 * boot_system.h
 *
 *  Created on: May 1, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef SYSTEM_H_
#define SYSTEM_H_

// Include  -------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Macros & constants  --------------------------------------------------------
#define FW_VERSION		"v1.0.0"	/* !< Current version of the firmware. */

// TODO move to api_config.h
#define OTB_APP_CODE 			/* !< Define used by the HAL to recognize that this code is for the bootloader. */

#endif /* SYSTEM_H_ */
