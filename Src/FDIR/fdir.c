/*
 * fdir.c
 *
 *  Created on: Aug 16, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "fdir_private.h"

// Macros & constants ---------------------------------------------------------
/* @brief system housekeeping task configuration parameters. */
#define FDIR_TASK_STATUS_PERIOD 30000	/*<! Period (in ms) in which the system housekeeping analyzes tasks status. */
#define FDIR_TASK_SANITY_LED_TOGGLE_PERIOD 500 /*<! Period (in ms) of the sanity led toggling (debug purposes). */
#define FDIR_TASK_FS_HEALTH_CHECK_PERIOD	60000 /*<! Period (in ms) of the sanity led toggling (debug purposes). */
#define FDIR_TASK_STACK_SIZE				1024
#define FDIR_TASK_PRIOTY		osPriorityNormal

#define FDIR_LOG_ENABLED			1	/*<! Enables module log. */

#define	LOG_TAG							"fdir"	/*<! Tag assigned to logs for this module. */

#if FDIR_LOG_ENABLED
	#define FDIR_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define FDIR_LOG_WARNING(format, ...)		LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define FDIR_LOG_INFO(format, ...)			LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define FDIR_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define FDIR_LOG_ERROR(...)
	#define FDIR_LOG_WARNING(...)
	#define FDIR_LOG_INFO(...)
	#define FDIR_LOG_DEBUG(...)
#endif

#define FDIR_CHECK_FILE_DIR	"/health_test" /*!< Directory where the health_test file is created. */
#define FDIR_CHECK_FILE_NAME	"/health.txt" /*!< Name used for the test file on the Disk. */
#define FDIR_CHECK_FILE_PATH	FDIR_CHECK_FILE_DIR FDIR_CHECK_FILE_NAME

// Private variables declaration ----------------------------------------------
typedef struct
{
	struct
	{
		TickType_t get_task_status_info;
		TickType_t sanity_led_toggle;
		TickType_t fs_check;
		TickType_t now;
	}ts;
	TaskHandle_t task_handle;
} fdir_t;

// Private variables definition -----------------------------------------------
static fdir_t fdir;

// Private functions declaration ----------------------------------------------
static ret_code_t fdir_check_fs(void);
void fdir_task(void *arg);

// Public functions definition ------------------------------------------------
TaskHandle_t fdir_init(void *arg)
{
	if( xTaskCreate(	fdir_task, "fdir",
						FDIR_TASK_STACK_SIZE,
						NULL,
						FDIR_TASK_PRIOTY,
						&fdir.task_handle ) != pdPASS )
	{
		/* If the task can't be created the task handle will be NULL. */
		fdir.task_handle = NULL;
	}

	return fdir.task_handle;
}

// Private functions definition -----------------------------------------------
/*
 * @brief system housekeeping task.
 * */
void fdir_task(void *arg)
{
	int now = 0;
	/* Make an array with all the tasks executing on the system. */
	UBaseType_t uxArraySize = 0;
	TaskStatus_t *pxTaskStatusArray = NULL;
	uxArraySize = uxTaskGetNumberOfTasks();
	pxTaskStatusArray = pvPortMalloc(uxArraySize*sizeof(TaskStatus_t));
	if( NULL != pxTaskStatusArray ) uxTaskGetSystemState(pxTaskStatusArray, uxArraySize, NULL);

	while(true)
	{
		now = TICKS_TO_MS(xTaskGetTickCount());
		if( FDIR_TASK_STATUS_PERIOD < now-fdir.ts.get_task_status_info )
		{	/* Log the free stack of each task. */
			if( NULL != pxTaskStatusArray )
			{
				for( int x = 0; x < uxArraySize; x++ )
				{
					/* Don't include the state of the task in the query (takes too long and we don't
					 * need it). The high watermark takes too long too, but is an important parameter to track.
					 * Just mantain high the period of this check (30 secs at least). */
					vTaskGetInfo(pxTaskStatusArray[x].xHandle, &pxTaskStatusArray[x], pdTRUE, eSuspended);
					FDIR_LOG_INFO("%s: %s f:%d", __func__,
										 pxTaskStatusArray[x].pcTaskName,
										 pxTaskStatusArray[x].usStackHighWaterMark);
				}
				fdir.ts.get_task_status_info = now;
			}
		}


		if( FDIR_TASK_SANITY_LED_TOGGLE_PERIOD < now-fdir.ts.sanity_led_toggle )
		{	/* Toggle led to debug sanity on the application. */
			API_GPIO_Toggle(API_GPIO_DBG_LED1);
			fdir.ts.sanity_led_toggle = now;
		}

		if( FDIR_TASK_FS_HEALTH_CHECK_PERIOD < now-fdir.ts.fs_check )
		{ /* Run filesystem check. */
			fdir_check_fs();
			fdir.ts.fs_check = now;
		}

		vTaskDelay(50/portTICK_RATE_MS);
	}
	if( NULL != pxTaskStatusArray ) vPortFree(pxTaskStatusArray);
}

/*
 * @brief check the file system of the available volumes.
 * @return RET_OK if success.
 * */
static ret_code_t fdir_check_fs(void)
{
	ret_code_t ret = RET_OK;
	API_FS_statvfs_t statvfs = {0};
	char str_buff[128] = {0};
    char str_ts[API_DATETIME_TS_STR_LEN];
    char path[API_FS_DRIVE_STR_MAX_LEN+sizeof(FDIR_CHECK_FILE_PATH)+5] = {0};
    uint32_t readed_bytes_testfile = 0;	/* will store the bytes readed from the test file. */
	char *volumes_to_check[] =
	{
#ifdef API_FS_USE_SD
			API_FS_DISK_PATH,
#endif
#ifdef API_FS_USE_MB85RS
			API_FS_FRAM0_DISK_PATH
#endif
	};
	uint8_t vol_count = sizeof(volumes_to_check)/sizeof(volumes_to_check[0]);

	for(int i = 0; i<sizeof(vol_count); i++)
	{
		if( 0 == API_FS_mount(volumes_to_check[i]) )
		{
			/* First check the free space. */
			ret = API_FS_statvfs(volumes_to_check[i], &statvfs);
			if( RET_OK != ret )
			{
				FDIR_LOG_ERROR("%s:fs check err: retval=%d",__func__, ret);
			}
			else
			{
				FDIR_LOG_INFO(	"%s:fs check ok: space: %10lu/%10lu B", __func__,
										statvfs.free_sectors*statvfs.sector_size,
										statvfs.total_sectors*statvfs.sector_size);
			}
			/* Then check if we can write to a file. */
			sprintf(path, "%s%s", volumes_to_check[i], FDIR_CHECK_FILE_DIR);
			if( 0 == API_FS_mkdir(path) )
			{
				API_FS_FILE *test_file = NULL;
				sprintf(path, "%s%s", volumes_to_check[i], FDIR_CHECK_FILE_PATH);
				/* First open the file to read its content. Its more for log purposes. */
				test_file = API_FS_fopen(path, "r");
				if( NULL != test_file )
				{
					readed_bytes_testfile = API_FS_fread(str_buff, sizeof(str_buff[0]), sizeof(str_buff), test_file);
					str_buff[readed_bytes_testfile] = '\0';
					FDIR_LOG_INFO("%s: old health %s%s check: %s", __func__, volumes_to_check[i], FDIR_CHECK_FILE_PATH, str_buff);
					if( 0 != API_FS_fclose(test_file) )
					{
						FDIR_LOG_ERROR("%s: error opening %s%s", __func__, volumes_to_check[i], FDIR_CHECK_FILE_PATH);
					}
				}
				/* Then overwrite the old text with the new one. */
				test_file = API_FS_fopen(path, "w");
				if( NULL != test_file )
				{
					/* Write the new data to health file. */
					API_Datetime_Get_Timestamp(str_ts, sizeof(str_ts));
					sprintf(str_buff,"%s: healthy fs check\r\n", str_ts);
					API_FS_fwrite(str_buff, sizeof(str_buff[0]), strlen(str_buff), test_file);
					FDIR_LOG_INFO("%s: new health %s%s check: %s", __func__, volumes_to_check[i], FDIR_CHECK_FILE_PATH, str_buff);
					if( 0 == API_FS_fclose(test_file) )
					{
						/* TODO handle this error. */
					}
				}
			}

			API_FS_umount(volumes_to_check[i]);
		}
	}
	return ret;
}
