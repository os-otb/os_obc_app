/*
 * fdir_private.h
 *
 *  Created on: Aug 17, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef FDIR_PRIVATE_H_
#define FDIR_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "fdir.h"

#include "api.h"	/* !< OBCs API include. */

#endif /* FDIR_FDIR_PRIVATE_H_ */
