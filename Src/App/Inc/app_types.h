/*
 * app_types.h
 *
 *  Created on: Aug 19, 2021
 *      Author: marifante
 */

#ifndef APP_TYPES_H_
#define APP_TYPES_H_

// Macros & Constants ---------------------------------------------------------

// Public variable declaration ------------------------------------------------
typedef enum
{
	APP_MSG_DEFAULT = 0,
	APP_MSG_NET_SV_STATUS,	 	/*!< Message sent by the a net server task with the server status!. */
}app_msg_id_t;

/* @brief struct sent by a server task to the app task with the status of the server. */
typedef struct
{
	net_server_id_t sv_id;
	bool sv_up;		/*!< The app only wants to know if the server is up (true) or if it is down (false). */
} app_msg_sv_status_t;

/**
 * @brief message packet format sent to the boot app task.
 * The data field is an union with the different data packets that can be sent
 * to the task.
 */
typedef struct //STRUCT_PACKET
{
	app_msg_id_t id;
	union
	{
		app_msg_sv_status_t sv_status;
	}data;
}app_msg_t;


#endif /* APP_INC_APP_TYPES_H_ */
