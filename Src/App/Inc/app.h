/*
 * boot_app.h
 *
 *  Created on: May 19, 2021
 *      Author: marifante
 */

#ifndef APP_H_
#define APP_H_

// Include --------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Bootloader modules types
#include "net_types.h"
#include "app_types.h"

// Public functions declaration  ----------------------------------------------
TaskHandle_t app_init(void *arg);

/* Functions used by other tasks to send msgs to the app task. */
ret_code_t app_send_msg_sv_status(app_msg_sv_status_t sv_status); /* !< Used by a server task to send information about it status to the app task. */

#endif /* BOOT_APP_H_ */
