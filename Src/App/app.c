/*
 * board.c
 *
 *  Created on: March 15, 2021
 *      Authors:
 *      Lucas Mancini
 *      Julian Rodriguez aka marifante
 */

// Include --------------------------------------------------------------------
#include "app_private.h" 	/* !< App module private include. */

// Macros & constants ---------------------------------------------------------
#define APP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define APP_TASK_STACK_SIZE		512
#define APP_TASK_PRIOTY			osPriorityNormal
#define APP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define APP_QUEUE_LEN				10

// Private functions declaration ----------------------------------------------
void app_task(void *arg);
ret_code_t app_fsm_init_ongoing_handler(void *arg);
ret_code_t app_fsm_init_on_entry_handler(void *arg);

// Private variables definition -----------------------------------------------
/* States of the fsm of this module: */
api_fsm_state_t app_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	APP_STATE_INIT,
							app_fsm_init_on_entry_handler,
							app_fsm_init_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(APP_STATE_IDLE,
							NULL,
							NULL,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t app_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_INIT, APP_EV_RETRY_INIT, APP_STATE_INIT),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(APP_STATE_INIT, APP_EV_DEV_CONN, APP_STATE_IDLE),
};

// Public functions definition  -----------------------------------------------
/**
 * @brief init app module of bootloader.
 * @return RET_OK if success.
 * */
TaskHandle_t app_init(void *arg)
{
	/* Create commands queue and task: */
	app.cmd_queue = xQueueCreate(APP_QUEUE_LEN, sizeof(app_msg_t));
	if( NULL != app.cmd_queue )
	{
		if( xTaskCreate(	app_task, "app",
							APP_TASK_STACK_SIZE,
							NULL,
							APP_TASK_PRIOTY,
							&app.task_handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			app.task_handle = NULL;
		}
	}
	return app.task_handle;
}

// Private functions definition -----------------------------------------------
void app_task(void *arg)
{
	app_msg_t msg;

	while(true)
	{
		if(pdTRUE == xQueueReceive(app.cmd_queue, (uint8_t*)&msg, APP_QUEUE_TIMEOUT))
		{
			app_process_msg(&msg); // process incoming message
		}
	}
}

/* Definition of the handlers of the fsm: */
/* APP_FSM_STATE_INIT functions ----------------------------------------*/

/*
 * @brief sends a message to the net task saying it must init the ftp server!
 * */
ret_code_t app_fsm_init_on_entry_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	app.fsm_extended_vars.init.entry_time = TICKS_TO_MS(xTaskGetTickCount());

	ret = net_send_msg_query_sv_status(NET_FTP_SERVER_ID);
	if( RET_OK != ret )
	{
		APP_LOG_ERROR("%s: error sending message to net ftp task");
	}

	ret = net_send_msg_query_sv_status(NET_OSOTB_SERVER_ID);
	if( RET_OK != ret )
	{
		APP_LOG_ERROR("%s: error sending message to net osotb task");
	}

	if( (true == app.fsm_extended_vars.init.sv_status.ftp_up) &&
		(true == app.fsm_extended_vars.init.sv_status.osotb_up) )
	{
		APP_LOG_INFO("%s: The servers are up! continue.", __func__);
		api_fsm_process_event(&app.fsm, APP_EV_DEV_CONN, NULL);
	}

	return ret;
}

/*
 * @brief if the timeout has been reached, retry the init process. An event
 * triggered from the net task will send this fsm to the idle state.
 * */
ret_code_t app_fsm_init_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	int now = TICKS_TO_MS(xTaskGetTickCount());
	if( APP_INIT_STATE_RETRY_TIMEOUT_MS < (now - app.fsm_extended_vars.init.entry_time) )
	{
		ret = api_fsm_process_event(&app.fsm, APP_EV_RETRY_INIT, NULL);
	}
	return ret;
}
