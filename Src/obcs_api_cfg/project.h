/*
 * project.h
 *
 *  Created on: Aug 5, 2021
 *      Author: julian alias Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef PROJECT_H_
#define PROJECT_H_

/*
 * With this definition, one can compile the project for the OpenSpace payload
 * or for the OTB.
 * */
#define OTB

#endif /* OBC_API_CFG_PROJECT_H_ */
