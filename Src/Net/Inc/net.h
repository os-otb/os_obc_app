/*
 * net.h
 *
 *  Created on: May 24, 2021
 *      Author: marifante
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 *
 */

#ifndef NET_H_
#define NET_H_

// Include --------------------------------------------------------------------
#include "api_types.h"
#include "rtos.h"

// Bootloader modules types
#include "net_types.h"
#include "app_types.h"

// Public functions declaration  ----------------------------------------------
TaskHandle_t net_init(void *arg);
/* Functions used by other tasks to send msgs to the server tasks. */
ret_code_t net_send_msg_net_up_event(net_msg_data_net_up_event_t net_info, net_server_id_t sv_id);
ret_code_t net_send_msg_net_down_event(net_server_id_t sv_id);
ret_code_t net_send_msg_ping_response(net_msg_data_ping_response_t ping_response_info, net_server_id_t sv_id);
ret_code_t net_send_msg_query_sv_status(net_server_id_t sv_id);

#endif /* NET_H_ */
