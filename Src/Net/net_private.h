/*
 * net_private.h
 *	This module can create two servers:
 *	1. A FTP server to receive one firmware binary.
 *	2. A OSTB server to receive commands.
 *	Each server is handled with two different task. This tasks shares
 *	the same skeleton and only differs in the parameter passed as argument in its creation.
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef NET_PRIVATE_H_
#define NET_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "net.h"
#include "app.h"

#include "api.h"	/* !< OBCs API include. */

// Macros and constants -------------------------------------------------------
// Private variables declaration ----------------------------------------------
typedef struct
{
	struct
	{
		bool bin_rx;	/*!< Set by the task when a binary has been received. */
		char filename[API_FS_FILE_NAME_MAX_LEN];	/*!< The name of the received file. */
	}sv_work;
}ftp_fsm_extended_vars_t;

typedef struct
{
	net_server_id_t sv_id;		/* !< With this ID we can identify the server in the table of servers. */
	api_fsm_t fsm;					/*!< The fsm of the server. */
	struct
	{
		TaskHandle_t handle;		/* !< The task handle of the task that manage the server. */
		QueueHandle_t cmd_queue;  	/* !< Other tasks or ISR will notify the server net task about some events with this queue. */
		TickType_t queue_timeout;	/* !< The time that the server task is blocked waiting some message from another task. */
	}task;

	/* This struct have the extended vars of the states of the fsm. With this vars, certain
	 * states of the fsm will do some operations to evaluate the flow of the operation.
	 * It's an union because there can be the extended vars of the ftp server or another servers. */
	union
	{
		ftp_fsm_extended_vars_t ftp;	/*!< Extender vars of the fpt fsm. */
		/* TODO: the OSOTB server have extended vars? */
	}fsm_extended_vars;
} net_server_t;

/**
 * @brief message packet format sent to the boot net tasks.
 * The data field is an union with the different data packets that can be sent
 * to the task.
 */
typedef struct //STRUCT_PACKET
{
	net_msg_id_t id;		/* !< The ID of the message. */
	net_server_id_t sv_id;	/* !< The ID of the destiny server of this packet. */
	union
	{
		net_msg_data_net_up_event_t net_up_event;
		net_msg_data_ping_response_t ping_response;
		union
		{
			net_ftp_msg_send_data_t ftp;
		}send_data;

	}data;
}net_msg_t;

typedef struct
{
  uint8_t id;
  void (*handler)(net_msg_t *msg);
}net_process_msg_table_t;

/* @brief the states of each server fsm. */
typedef enum
{
	NET_FSM_SV_DISCONNECTED = 0,	/*!< Server knows is disconnected from the net. */
	NET_FSM_SV_CREATE,				/*!< Server task is trying to create the server. */
	NET_FSM_SV_WORK,				/*!< Server is making its normal work cycle (wait until receive and process). */
	NET_FSM_SV_SEND,				/*!< Server is sending some data to it client. */
	NET_FSM_MAX_STATES,	/*!< Max states. */
}net_fsm_state_t;

/*
 * @brief events that will trigger transitions in the server fsms:
 * */
typedef enum
{
	NET_EV_SV_CONNECTED = 0,		/*!< The device is connected to the network! */
	NET_EV_SV_DISCONNECTED,		/*!< The device was disconnected from the network! */
	NET_EV_SV_CREATED,				/*!< The server was created successfully! */
	NET_EV_SV_SEND_DATA_REQUEST,	/*!< Another task wants to send data to the client. */
	NET_EV_SV_DATA_TX_SUCCESS,		/*!< A data was sent successfully to the client. */
	NET_EV_SV_DATA_TX_TIMEOUT,		/*!< A data transmissions wasn't successfull and timeout has ocurred. */
    NET_MAX_EVENTS					/*!< Max events on the fsm. */
}app_fsm_state_events_t;

// Private variables definition -----------------------------------------------
net_server_t net_ftp;
net_server_t net_osotb;

// Private functions declaration ----------------------------------------------
void net_task_template(void *arg); /* !< Task function template used by the servers. */
void net_process_msg(net_msg_t *msg);

ret_code_t net_ftp_init(void);
ret_code_t net_osotb_init(void);

/* msg handlers of FTP server. */
void net_ftp_process_msg_net_up_event(net_msg_t *msg);
void net_ftp_process_msg_net_down_event(net_msg_t *msg);
void net_ftp_process_msg_send_file(net_msg_t *msg);
void net_ftp_process_msg_query_sv_status(net_msg_t *msg);

/* msg handlers of OSOTB server. */
void net_osotb_process_msg_net_up_event(net_msg_t *msg);
void net_osotb_process_msg_net_down_event(net_msg_t *msg);
void net_osotb_process_msg_query_sv_status(net_msg_t *msg);

#endif /* NET_PRIVATE_H_ */
