/*
 * net_msg.c
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "net_private.h" 	/* !< Net module private include. */

// Private functions declaration ----------------------------------------------
ret_code_t net_send_msg(net_msg_t *msg);

// Boot Net Task message-handler table ----------------------------------------
/* Net Task Messages handlers table: */
#define NET_PROCESS_MSG(x,y) {.id = x, .handler=y}

/* Msgs table of FTP server task... */
static net_process_msg_table_t net_ftp_process_msg_table[] =
{
  NET_PROCESS_MSG(NET_MSG_DEFAULT, NULL),
  NET_PROCESS_MSG(NET_MSG_NET_UP_EVENT, net_ftp_process_msg_net_up_event),
  NET_PROCESS_MSG(NET_MSG_NET_DOWN_EVENT, net_ftp_process_msg_net_down_event),
  NET_PROCESS_MSG(NET_MSG_SEND, net_ftp_process_msg_send_file),
  NET_PROCESS_MSG(NET_MSG_QUERY_SV_STATUS, net_ftp_process_msg_query_sv_status)
};
#define SIZEOF_NET_FTP_PROCESS_MSG_TABLE (sizeof(net_ftp_process_msg_table) / sizeof(net_ftp_process_msg_table[0]))

/* Msgs table of OSOTB server task... */
static net_process_msg_table_t net_osotb_process_msg_table[] =
{
  NET_PROCESS_MSG(NET_MSG_DEFAULT, NULL),
  NET_PROCESS_MSG(NET_MSG_NET_UP_EVENT, net_osotb_process_msg_net_up_event),
  NET_PROCESS_MSG(NET_MSG_NET_DOWN_EVENT, net_osotb_process_msg_net_down_event),
  NET_PROCESS_MSG(NET_MSG_SEND, NULL),	// TODO: this must be implemented?
  NET_PROCESS_MSG(NET_MSG_QUERY_SV_STATUS, net_osotb_process_msg_query_sv_status)
};
#define SIZEOF_NET_OSOTB_PROCESS_MSG_TABLE (sizeof(net_osotb_process_msg_table) / sizeof(net_osotb_process_msg_table[0]))


// Public functions definition  -----------------------------------------------
/* Functions used by other tasks to "talk" with the net tas. */
/**
 * @brief function used by FreeRTOS+TCP TCP/IP stack to notify
 * to the net task that the device was recently connected to the network.
 */
ret_code_t net_send_msg_net_up_event(net_msg_data_net_up_event_t net_info, net_server_id_t sv_id)
{
	net_msg_t msg;
	msg.sv_id = sv_id;
	msg.id = NET_MSG_NET_UP_EVENT;
	memcpy(&msg.data.net_up_event, &net_info, sizeof(net_info));
	return net_send_msg(&msg);
}

/**
 * @brief function used by FreeRTOS+TCP TCP/IP stack to notify
 * to the net task that the device was recently connected to the network.
 */
ret_code_t net_send_msg_net_down_event(net_server_id_t sv_id)
{
	net_msg_t msg;
	msg.sv_id = sv_id;
	msg.id = NET_MSG_NET_DOWN_EVENT;
	return net_send_msg(&msg);
}

/**
 * @brief function used by FreeRTOS+TCP TCP/IP stack to notify
 * to the net task the result of a ping sent previously by the device.
 */
ret_code_t net_send_msg_ping_response(net_msg_data_ping_response_t ping_response_info, net_server_id_t sv_id)
{
	net_msg_t msg;
	msg.sv_id = sv_id;
	msg.id = NET_MSG_PING_RESPONSE;
	memcpy(&msg.data.ping_response, &ping_response_info, sizeof(ping_response_info));
	return net_send_msg(&msg);
}

/**
 * @brief function used by other tasks to query to the server task the server status.
 */
ret_code_t net_send_msg_query_sv_status(net_server_id_t sv_id)
{
	net_msg_t msg;
	msg.sv_id = sv_id;
	msg.id = NET_MSG_QUERY_SV_STATUS;
	return net_send_msg(&msg);
}

/**
 * @brief send a message to a net server task through its queue.
 * This function can be called from an ISR too.
 */
ret_code_t net_send_msg(net_msg_t *msg)
{
	BaseType_t rtos_result = pdTRUE;
	ret_code_t res = RET_OK;
	QueueHandle_t queue = NULL;
	if( NULL == msg ) return RET_ERR_NULL_POINTER;

	/* Choose between the server queues. */
	switch(msg->sv_id)
	{
		case NET_FTP_SERVER_ID:
			queue = net_ftp.task.cmd_queue;
			break;
		case NET_OSOTB_SERVER_ID:
			queue = net_osotb.task.cmd_queue;
			break;
		default:
			break;
	}

	if( NULL == queue ) return RET_ERR_NULL_POINTER;

	if( pdTRUE != xPortIsInsideInterrupt() )
	{
		/* If the core isn't in an interrupt context then send the msg normally. */
		rtos_result = xQueueSend( queue, msg, ( TickType_t ) 10 );
	}
	else
	{
			/* If the core is in an interrupt context then use send from ISR API. */
		portBASE_TYPE xHigherPriorityTaskWoken;
		BaseType_t yield_req = pdFALSE;

		rtos_result = xQueueSendFromISR( queue, msg, &xHigherPriorityTaskWoken );
		// Now the buffer is empty we can switch context if necessary.
		if( xHigherPriorityTaskWoken )
		{
		  // Actual macro used here is port specific.
		  portYIELD_FROM_ISR(yield_req);
		}
	}
	res = (pdTRUE == rtos_result) ? RET_OK : RET_ERR;
	return res;
}

// Private functions definition -----------------------------------------------
/**
 * @brief  process incoming queue messages. This function calls the handler
 * associated with that message.
 * @param msg queue message
 */
void net_process_msg(net_msg_t *msg)
{
	if( NULL == msg ) return;
	uint8_t c;
	net_process_msg_table_t *msg_table = NULL;
	int table_size = 0;
	/* Choose between the server queues. */
	switch(msg->sv_id)
	{
		case NET_FTP_SERVER_ID:
			msg_table = net_ftp_process_msg_table;
			table_size = SIZEOF_NET_FTP_PROCESS_MSG_TABLE;
			break;
		case NET_OSOTB_SERVER_ID:
			msg_table = net_osotb_process_msg_table;
			table_size = SIZEOF_NET_OSOTB_PROCESS_MSG_TABLE;
			break;
		default:
			break;
	}

	if( NULL != msg_table )
	{
		for(c = 0; c < table_size; c++)
		{
			if(msg_table[c].id == msg->id)
			{
				if(NULL != msg_table[c].handler) msg_table[c].handler(msg); // found, check callback
				break;	// finish search
			}
		}
	}
}

