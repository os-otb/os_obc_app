/*
 * net_ftp.c
 *
 *  Created on: Aug 18, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "net_private.h" 	/* !< Net module private include. */

// Macros & Constants ---------------------------------------------------------
#define NET_FTP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define NET_FTP_TASK_STACK_SIZE		1024
#define NET_FTP_TASK_PRIOTY			osPriorityNormal
#define NET_FTP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define NET_FTP_QUEUE_LEN				10

/* Network config. TODO move to another place? */
#define NET_FTP_SERVER_PORT				49152
#define NET_FTP_LOG_ENABLED				1		/*<! Enables module log. */
#define	LOG_TAG						"net_ftp"	/*<! Tag assigned to logs for this module. */

#if NET_FTP_LOG_ENABLED
	#define NET_FTP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_FTP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_FTP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_FTP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define NET_FTP_LOG_ERROR(...)
	#define NET_FTP_LOG_WARNING(...)
	#define NET_FTP_LOG_INFO(...)
	#define NET_FTP_LOG_DEBUG(...)
#endif

// Private functions declaration ----------------------------------------------
ret_code_t net_ftp_fsm_sv_work_ongoing_handler(void *arg);
ret_code_t net_ftp_fsm_sv_create_ongoing_handler(void *arg);
ret_code_t net_ftp_fsm_sv_send_ongoing_handler(void *arg);

// Private variables declaration ----------------------------------------------
/* States of the fsm of this module: */
api_fsm_state_t net_ftp_fsm_states[] =
{
	API_FSM_DEFINE_STATE(	NET_FSM_SV_DISCONNECTED,
							NULL,
							NULL,
							NULL),
	API_FSM_DEFINE_STATE(	NET_FSM_SV_CREATE,
							NULL,
							net_ftp_fsm_sv_create_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	NET_FSM_SV_WORK,
							NULL,
							net_ftp_fsm_sv_work_ongoing_handler,
							NULL),
	API_FSM_DEFINE_STATE(	NET_FSM_SV_SEND,
							NULL,
							net_ftp_fsm_sv_send_ongoing_handler,
							NULL),
};

/* Transitions of the fsm of this module: */
api_fsm_transition_t net_ftp_fsm_transitions_map[] =
{
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_DISCONNECTED, NET_EV_SV_CONNECTED, NET_FSM_SV_CREATE),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_CREATE, NET_EV_SV_CREATED, NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_WORK, NET_EV_SV_SEND_DATA_REQUEST, NET_FSM_SV_SEND),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_SEND, NET_EV_SV_DATA_TX_SUCCESS, NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_SEND, NET_EV_SV_DATA_TX_TIMEOUT, NET_FSM_SV_WORK),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_SEND, NET_EV_SV_DISCONNECTED, NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_WORK, NET_EV_SV_DISCONNECTED, NET_FSM_SV_DISCONNECTED),
	API_FSM_DEFINE_TRANSITION_MAP_ITEM(NET_FSM_SV_CREATE, NET_EV_SV_DISCONNECTED, NET_FSM_SV_DISCONNECTED),
};

// Private variables definition -----------------------------------------------

// Public functions definition  -----------------------------------------------
/*
 * @brief initalize the ftp server task.
 * @return RET_OK if success.
 * */
ret_code_t net_ftp_init(void)
{
	ret_code_t ret = RET_ERR;
	memset(&net_ftp, 0, sizeof(net_ftp));
	net_ftp.sv_id = NET_FTP_SERVER_ID;

	net_ftp.fsm.actual_state = &net_ftp_fsm_states[0];
	net_ftp.fsm.previous_state = &net_ftp_fsm_states[0];
	net_ftp.fsm.state_table = net_ftp_fsm_states;
	net_ftp.fsm.transition_map = net_ftp_fsm_transitions_map;
	net_ftp.fsm.states_qty = NET_FSM_MAX_STATES;
	net_ftp.fsm.transitions_qty = NET_MAX_EVENTS;

 	net_ftp.task.cmd_queue = xQueueCreate(NET_FTP_QUEUE_LEN, sizeof(net_msg_t));
	if( NULL != net_ftp.task.cmd_queue )
	{
		// TODO check if the queue was created
		if( xTaskCreate(	net_task_template, "net_ftp",
							NET_FTP_TASK_STACK_SIZE,
							&net_ftp,	/*!< Pass a pointer to the server struct to the task function to handle the server fsm. */
							NET_FTP_TASK_PRIOTY,
							&net_ftp.task.handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			net_ftp.task.handle = NULL;
		}
		else
		{
			ret = RET_OK;
		}
	}
	return ret;
}

// Private functions definition -----------------------------------------------
/**
 * @brief callback of the FTP API. This callback is called every time that a
 * file is received!.
 * @param file_name is the absolute path of the file.
 * @param reply_msg reply message is the
 */
void api_ftp_file_received_cb(const char *file_name, char *reply_msg)
{
	if( NULL != file_name )
	{
	    if( ( 0 == strcmp(file_name, API_IAP_APP0_ABS_PATH)) ||
			( 0 == strcmp(file_name, API_IAP_APP1_ABS_PATH)) )
	    { /* If the file is a binary file with a valid name. */
	      net_ftp.fsm_extended_vars.ftp.sv_work.bin_rx = true;
	      strcpy(net_ftp.fsm_extended_vars.ftp.sv_work.filename, file_name);
	      sprintf(reply_msg, "200-Binary %s received!\r\n", file_name);
	      NET_FTP_LOG_INFO("%s: valid binary received! (%s)", __func__, file_name);
	    }
	    else
	    {
	      // TODO: delete file?
		  sprintf(reply_msg, "200-Incorrect filename (%s)!", file_name);
	      NET_FTP_LOG_WARNING("%s: the binary received is not a valid binary for flash (%s)!", __func__, file_name);
	    }
	}
}

/* Task Message handlers ----------------------------------------------------*/
/**
 * @brief  process NET_MSG_NET_DOWN_EVENT message.
 * @param msg queue message
 */
void net_ftp_process_msg_net_down_event(net_msg_t *msg)
{
	NET_FTP_LOG_WARNING("Device disconnected from the network.");
	/* Trigger the event that says to the FSM that the device is disconnected
	 * from the network. */
	api_fsm_process_event(&net_ftp.fsm, NET_EV_SV_DISCONNECTED, NULL);
}

/**
 * @brief  process NET_MSG_NET_UP_EVENT message.
 * @param msg queue message
 */
void net_ftp_process_msg_net_up_event(net_msg_t *msg)
{
	api_fsm_process_event(&net_ftp.fsm, NET_EV_SV_CONNECTED, NULL);
}

/**
 * @brief process a query of the status of the server.
 */
void net_ftp_process_msg_query_sv_status(net_msg_t *msg)
{
	if( NULL != msg )
	{
		if( (NET_FSM_SV_WORK == net_ftp.fsm.actual_state->id) ||
			(NET_FSM_SV_SEND == net_ftp.fsm.actual_state->id) )
		{
			app_msg_sv_status_t sv_status = {0};
			sv_status.sv_id = NET_FTP_SERVER_ID;
			sv_status.sv_up = true;
			app_send_msg_sv_status(sv_status);
		}
	}
	else
	{
		NET_FTP_LOG_ERROR("%s: msg is null!", __func__);
	}
}

/**
 * @brief process NET_MSG_QUERY_SV_STATUS message.
 * @param msg queue message
 * */
void net_ftp_process_msg_send_file(net_msg_t *msg)
{
	/*TODO implement this*/
}

/* FSM on_entry, ongoing and on_exit handlers -------------------------------*/
/*
 * @brief in this state, the task tries to create the server continuosly. And if
 * the server is created sucessfully, then go to the next state.
 * */
ret_code_t net_ftp_fsm_sv_create_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = API_FTP_Server_Create();
	if( RET_OK == ret )
	{
		NET_FTP_LOG_INFO("%s: FTP server created!", __func__);
		api_fsm_process_event(&net_ftp.fsm, NET_EV_SV_CREATED, NULL);
	}
	return ret;
}

/*
 * @brief do a ftp server working cycle.
 * */
ret_code_t net_ftp_fsm_sv_work_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	ret = API_FTP_Server_Work();
	if( RET_OK == ret )
	{

	}
	return ret;
}

/*
 * @brief N/A
 * TODO this must be implemented.
 * */
ret_code_t net_ftp_fsm_sv_send_ongoing_handler(void *arg)
{
	ret_code_t ret = RET_OK;
	// TODO implement this
	return ret;
}
