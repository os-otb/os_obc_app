/*
 * net_api_cb.c
 *
 *  Created on: Aug 17, 2021
 *      Author: Julian Rodriguez aka Marifante
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Includes -------------------------------------------------------------------
#include "net_private.h"

// Macros & Constants ---------------------------------------------------------
#define NET_API_CB_LOG_ENABLED				1		/*<! Enables module log. */
#define	LOG_TAG						"net_api_cb"	/*<! Tag assigned to logs for this module. */

#if NET_API_CB_LOG_ENABLED
	#define NET_API_CB_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_API_CB_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_API_CB_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define NET_API_CB_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define NET_API_CB_LOG_ERROR(...)
	#define NET_API_CB_LOG_WARNING(...)
	#define NET_API_CB_LOG_INFO(...)
	#define NET_API_CB_LOG_DEBUG(...)
#endif

// Network API callbacks ------------------------------------------------------
/*
 * @brief callback for the Network API. This callback called when the device
 * is disconnected from a network.
 * */
ret_code_t API_Network_Down_Event_Cb(void)
{
	ret_code_t ret = RET_OK;
	ret = net_send_msg_net_down_event(NET_FTP_SERVER_ID);
	return ret;
}

/*
 * @brief callback for the network API. This callback is called when the
 * device was connected to a network.
 * */
ret_code_t API_Network_Up_Event_Cb(API_Network_Data_Net_Up_Event_t ev_info)
{
	char c_buffer[16];
	FreeRTOS_inet_ntoa( ev_info.ulIPAddress, c_buffer );
	NET_API_CB_LOG_INFO("%s: IP Address: %s", __func__, c_buffer);

	FreeRTOS_inet_ntoa( ev_info.ulNetMask, c_buffer );
	NET_API_CB_LOG_INFO("%s: Subnet Mask: %s", __func__, c_buffer);

	FreeRTOS_inet_ntoa( ev_info.ulGatewayAddress, c_buffer );
	NET_API_CB_LOG_INFO("%s: Gateway Address: %s", __func__, c_buffer);

	FreeRTOS_inet_ntoa( ev_info.ulDNSServerAddress, c_buffer );
	NET_API_CB_LOG_INFO("%s: DNS Server Address: %s", __func__, c_buffer);

	ret_code_t ret = RET_OK;
	net_msg_data_net_up_event_t net_info = {0};
	net_info.ulDNSServerAddress = ev_info.ulDNSServerAddress;
	net_info.ulGatewayAddress = ev_info.ulGatewayAddress;
	net_info.ulGatewayAddress = ev_info.ulGatewayAddress;
	net_info.ulNetMask = ev_info.ulNetMask;

	ret = net_send_msg_net_up_event(net_info, NET_FTP_SERVER_ID);
	ret = net_send_msg_net_up_event(net_info, NET_OSOTB_SERVER_ID);
	return ret;
}

/*
 * @brief callback for the network API. This callback is called when a response
 * to a Ping is received. TODO: For now this is not used.
 * */
ret_code_t API_Network_Ping_Response_Cb(API_Network_Data_Ping_Response_t ev_info)
{
#if 0
	Boot_Net_Msg_Data_Ping_Response_T ping_response_info = {0};
	ping_response_info.echo_reply_id = ev_info.echo_reply_id;
	ping_response_info.status = ev_info.status;
	return Boot_Net_Send_Msg_Ping_Response(ping_response_info);
#endif
	return RET_OK;
}
