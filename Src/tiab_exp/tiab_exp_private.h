/*
 * tiab_exp_private.h
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

#ifndef TIAB_EXP_PRIVATE_H_
#define TIAB_EXP_PRIVATE_H_

// Includes -------------------------------------------------------------------
/* Tasks modules includes. */
#include "tiab_exp.h"

#include "api.h"	/* !< OBCs API include. */

// Macros and constants -------------------------------------------------------
/* Appwork config. TODO move to another place? */

#define TIAB_EXP_LOG_ENABLED			1		/*<! Enables module log. */

#define	LOG_TAG						"tiab_exp"	/*<! Tag assigned to logs for this module. */

#if TIAB_EXP_LOG_ENABLED
	#define TIAB_EXP_LOG_ERROR(format, ...)		LOG_ERROR(LOG_TAG, format, ##__VA_ARGS__)
	#define TIAB_EXP_LOG_WARNING(format, ...)	LOG_WARNING(LOG_TAG, format, ##__VA_ARGS__)
	#define TIAB_EXP_LOG_INFO(format, ...)		LOG_INFO(LOG_TAG, format, ##__VA_ARGS__)
	#define TIAB_EXP_LOG_DEBUG(format, ...)		LOG_DEBUG(LOG_TAG, format, ##__VA_ARGS__)
#else
	#define TIAB_EXP_LOG_ERROR(...)
	#define TIAB_EXP_LOG_WARNING(...)
	#define TIAB_EXP_LOG_INFO(...)
	#define TIAB_EXP_LOG_DEBUG(...)
#endif

// Private variables declaration ----------------------------------------------

typedef struct
{
	TaskHandle_t task_handle;
	QueueHandle_t cmd_queue;  	/* !< Other tasks or ISR will notify tiab_exp task about some events with this queue. */

} tiab_exp_t;

typedef struct
{
  uint8_t id;
  void (*handler)(tiab_exp_msg_t *msg);
}tiab_exp_process_msg_table_t;

// Private variables definition -----------------------------------------------
tiab_exp_t tiab_exp;

// Private functions declaration ----------------------------------------------
void tiab_exp_process_msg(tiab_exp_msg_t *msg);

// Module finite state machine declarations -----------------------------------

#endif /* TIAB_EXP_PRIVATE_H_ */
