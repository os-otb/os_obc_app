/*
 * tiab_exp.h
 *
 *  Created on: May 19, 2021
 *      Author: marifante
 */

#ifndef TIAB_EXP_H_
#define TIAB_EXP_H_

// Include --------------------------------------------------------------------
#include "tiab_exp_types.h"
#include "api_types.h"
#include "rtos.h"

// Bootloader modules types

// Public functions declaration  ----------------------------------------------
TaskHandle_t tiab_exp_init(void *arg);

/* Functions used by other tasks to send msgs to the tiab_exp task. */

#endif /* TIAB_EXP_H_ */
