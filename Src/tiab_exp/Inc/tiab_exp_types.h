/*
 * tiab_exp_types.h
 *
 *  Created on: Aug 19, 2021
 *      Author: marifante
 */

#ifndef TIAB_EXP_TYPES_H_
#define TIAB_EXP_TYPES_H_

// Macros & Constants ---------------------------------------------------------

// Public variable declaration ------------------------------------------------
typedef enum
{
  TIAB_EXP_MSG_DEFAULT = 0,

}tiab_exp_msg_id_t;

/**
 * @brief message packet format sent to the boot tiab_exp task.
 * The data field is an union with the different data packets that can be sent
 * to the task.
 */
typedef struct //STRUCT_PACKET
{
	tiab_exp_msg_id_t id;
//	union
//	{
//
//	}data;
}tiab_exp_msg_t;


#endif /* TIAB_EXP_TYPES_H_ */
