/*
 * tiab_exp.c
 *
 *  Created on: March 15, 2021
 *      Authors:
 *      Lucas Mancini
 *      Julian Rodriguez aka marifante
 */

// Include --------------------------------------------------------------------
#include "tiab_exp_private.h" 	/* !< App module private include. */

// Macros & constants ---------------------------------------------------------
#define TIAB_EXP_TASK_DELAY_MS			50			/*<! in ms, delay of the task. */
#define TIAB_EXP_TASK_STACK_SIZE		512
#define TIAB_EXP_TASK_PRIOTY			osPriorityNormal
#define TIAB_EXP_QUEUE_TIMEOUT			(10 / portTICK_PERIOD_MS)
#define TIAB_EXP_QUEUE_LEN				10

// Private functions declaration ----------------------------------------------
void tiab_exp_task(void *arg);

// Private variables definition -----------------------------------------------

// Public functions definition  -----------------------------------------------
/**
 * @brief init tiab_exp module of bootloader.
 * @return RET_OK if success.
 * */
TaskHandle_t tiab_exp_init(void *arg)
{
	/* Create commands queue and task: */
	tiab_exp.cmd_queue = xQueueCreate(TIAB_EXP_QUEUE_LEN, sizeof(tiab_exp_msg_t));
	if( NULL != tiab_exp.cmd_queue )
	{
		if( xTaskCreate(	tiab_exp_task, "tiab_exp",
							TIAB_EXP_TASK_STACK_SIZE,
							NULL,
							TIAB_EXP_TASK_PRIOTY,
							&tiab_exp.task_handle ) != pdPASS )
		{
			/* If the task can't be created the task handle will be NULL. */
			tiab_exp.task_handle = NULL;
		}
	}
	return tiab_exp.task_handle;
}

// Private functions definition -----------------------------------------------
void tiab_exp_task(void *arg)
{
	tiab_exp_msg_t msg;

	while(true)
	{
		if(pdTRUE == xQueueReceive(tiab_exp.cmd_queue, (uint8_t*)&msg, TIAB_EXP_QUEUE_TIMEOUT))
		{
			tiab_exp_process_msg(&msg); // process incoming message
		}
	}
}
