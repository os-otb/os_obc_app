/*
 * tiab_exp_msg.c
 *
 *  Created on: Jun 6, 2021
 *      Author: Julian Rodriguez aka Marifante 
 *		Email: jnrodriguezz@hotmail.com
 *		Gitlab: https://gitlab.com/Marifante
 *		Github: https://github.com/Marifante
 */

// Include --------------------------------------------------------------------
#include "tiab_exp_private.h" 	/* !< App module private include. */

// Private variables declaration ----------------------------------------------
static ret_code_t tiab_exp_send_msg(tiab_exp_msg_t *msg);
/* App Task Messages handlers prototypes: */
static void tiab_exp_process_msg_sv_status(tiab_exp_msg_t *msg);
static void tiab_exp_process_msg_binary_received(tiab_exp_msg_t *msg);

// Boot App Task message-handler table ----------------------------------------
/* App Task Messages handlers table: */
#define TIAB_EXP_PROCESS_MSG(x,y) {.id = x, .handler=y}
static const tiab_exp_process_msg_table_t tiab_exp_process_msg_table[] =
{
  TIAB_EXP_PROCESS_MSG(TIAB_EXP_MSG_DEFAULT, NULL),
};
#define SIZEOF_TIAB_EXP_PROCESS_MSG_TABLE (sizeof(tiab_exp_process_msg_table) / sizeof(tiab_exp_process_msg_table[0]))

// Public functions definition  -----------------------------------------------

/**
 * @brief send a message to the tiab_exp task through its queue.
 * This function can be called from an ISR too.
 * */
static ret_code_t tiab_exp_send_msg(tiab_exp_msg_t *msg)
{
	BaseType_t rtos_result = pdTRUE;
	ret_code_t res = RET_OK;

	if( (NULL == tiab_exp.cmd_queue) || (NULL == msg) )
	{
		return RET_ERR;
	}
	else
	{
		if(pdTRUE != xPortIsInsideInterrupt())
		{
			/* If the core isn't in an interrupt context then send the msg normally. */
			rtos_result = xQueueSend( tiab_exp.cmd_queue, msg, ( TickType_t ) 10 );
		}
		else
		{
				/* If the core is in an interrupt context then use send from ISR API. */
			portBASE_TYPE xHigherPriorityTaskWoken;
			BaseType_t yield_req = pdFALSE;

			rtos_result = xQueueSendFromISR( tiab_exp.cmd_queue, msg, &xHigherPriorityTaskWoken );
			// Now the buffer is empty we can switch context if necessary.
			if( xHigherPriorityTaskWoken )
			{
			  // Actual macro used here is port specific.
			  portYIELD_FROM_ISR(yield_req);
			}
		}
		res = (pdTRUE == rtos_result) ? RET_OK : RET_ERR;
		return res;
	}
}

// Private functions definition -----------------------------------------------
/**
 *
 * @brief  process incoming queue messages. This function calls the handler
 * associated with that message.
 *
 * @param msg queue message
 *
 */
void tiab_exp_process_msg(tiab_exp_msg_t *msg)
{
	uint8_t c;
	if( NULL != msg )
	{
		for(c = 0; c < SIZEOF_TIAB_EXP_PROCESS_MSG_TABLE; c++)
		{
			if(tiab_exp_process_msg_table[c].id == msg->id)
			{
				// found, check callback
				if(NULL != tiab_exp_process_msg_table[c].handler)
				{
					// call it
					tiab_exp_process_msg_table[c].handler(msg);
				}
				// finish search
				break;
			}
		}
	}
}
